Hi 👋, thanks for coming to read our Privacy Policy.
We’ve tried to make it as easy to read as possible, with no lawyer language.
We realise that when you use our websites and products you’re putting your faith in us to keep your data safe.
We take that responsibility seriously.
So as well as serving legal purposes, we hope that this Privacy Policy gives you some insight into how we do things which helps to strengthen your trust in how we handle your data. You may also like to view our [Security Policy](/security/).


The Short But Not Legally Binding Version
-----------------------------------------

If you just want a quick summary without reading the whole thing, here’s a version which has no legal meaning but which we think is a reasonable abbreviation:

* For users of AxioGrow’s Hiring Tool (Candidates): When you take the assessment, we store your answers and then show your results (but not the original answers) to the company who requested that you take the assessment. You may also wish to read our [Candidate FAQs](https://fidlleafknowledgebase-3660.notaku.site/candidate-fa-qs-c2208f03d8cc4129972a9614e695acc4) page. 
* For users of AxioGrow’s Wellbeing Tool: When you take the assessment, we store your answers and generate your results. If you took the assessment as part of the hiring process, then the company will have seen some of these results in the hiring report. If you only took the assessment through the wellbeing part of the platform then the company will not see your results. We will also show your employer/company an anonymised aggregated overview of the company/team results (so that they can see where common areas of need are), but we take care to protect the anonymity of the results, including by requiring a minimum number of people in the group.
* We use all of the **anonymised** assessment results for research. If you give us your gender and approximate date of birth then these are only used for this anonymised research. We may also store your approximate location alongside your assessment results for research purposes.
* We generally collect 3 types of other data:
    * Your personal details (for identifying and contacting you).
    * Data you’ve entered, e.g. notes you’ve typed, options you’ve selected (for making those features work).
    * Analytics data (so we can understand how the platform is used and improve it).
* We never sell or give away your personal information.
* We try to avoid looking at your personal information in our database, but sometimes we might need to (e.g. to diagnose a problem).
* We work hard to keep your data secure. We store it with trusted third party providers in the cloud.
* You can ask for a copy of your data, or for it to be deleted, at any time. For Candidates we recommend talking to the Customer you took the assessment through regarding your data.


The Full Version
----------------

This Privacy Policy applies to all websites under the [axiogrow.com](https://axiogrow.com) domain name, including [www.axiogrow.com](https://www.axiogrow.com) and [app.axiogrow.com](https://app.axiogrow.com).


Definitions
-----------

Here’s what mean when we use these words or terms in this document:

* **Us/we/our/AxioGrow**: AxioGrow Ltd, the UK company with registered company number 12364269 and registered address 227a West Street, Fareham, Hampshire PO16 0HZ.
* **Customer**: someone who enters into an agreement with us for use of the Platform, or who is given access to the Customer features of the Platform.
* **User**: someone who accesses the wellbeing features of the Platform.
* **Candidate**: someone who accesses the hiring assessment of the Platform.
* **Platform**: the web application (i.e. website), which is available at [app.axiogrow.com](https://app.axiogrow.com) and which contains our assessment tools, personal development content and personal development tools.
* **Subscription**: an agreement that allows a limited number of Users and/or Customers to access the Platform's wellbeing features for a specific period of time, or that allows one or more Customers to access hiring reports of a limiting number of Candidates.
* **Our Services**: any combination of: the Platform, access to the Platform, features of the Platform, our wellbeing workshops, or any other products or services which we sell or provide.

Customer, User and Candidate are not mutually exclusive roles.


Overarching principles
----------------------

* **We treat your data like nuclear waste.** We don’t want to be holding onto any more of it than we need to, and we keep it securely locked up, because if it leaks out then there’s no way to get it back and it’ll be hanging around out there for years to come.
* **We don’t sell your personal data.** Our business model is to create high quality products which our customers pay us for because they love them. We don’t sell your email address to third parties. No Viagra marketing emails. Not through us, at least.
    * Note that we use the _anonymised_ results of our assessments for research purposes (see **Assessment Data**).


Ok, let’s get into the details…


Data that we collect and why
----------------------------

We collect various data across our websites and systems. Here’s a breakdown of what data we collect and why we collect it.


### Data we collect on our website at www.axiogrow.com

On our website we may collect:

**Your name and email address.**
You may submit these details to us in order to join our mailing list or to receive materials from us (e.g. a wellbeing guide).
These are used for contacting you and for personalising those correspondence.

**Customer type and organisation name.**
You may give us an indication of what type of subscription(s) you are interested in (individual or company) and possibly your company or organisation name.
We use these to guide which type of correspondence or materials (e.g. a wellbeing guide) we send you.


### Data we collect during the purchasing process

During the process of you purchasing or inquiring about a Subscription or Our Services, whether via our website or other means, we may collect from you:

**Your name and email address.**
These are used for contacting you, for personalising those correspondence and for uniquely identifying you as a customer in our systems.

**Organisation name.**
This is used to customise functionality or wording in the Platform or in correspondence, or to help us identify your account.

**Your payment/billing address.**
This is collected automatically by our payment provider (Stripe). We don’t actively use it.

Note that we do not store your card details (see **Data that we don't collect**).

**Promotion codes.**
If you use a promotion/discount code when making your purchase, that code may be linked to a third party or a particular promotion, and informs us of where your purchase originated.
Some promotion codes may be linked to a third party whom we give a financial reward to when the code is used with a purchase.


Data we collect on the AxioGrow Platform
----------------------------------------

### If you are a Candidate, User or Customer, we may collect from you:

**Your name and email address.**
These are used to contact you and to uniquely identify you in our systems.
However, as a general policy we do not contact candidates directly.

**Your visits to and interactions with the Platform.**
When you view a page or take an action on the Platform we may store a record of that action including what it was and when it happened, as well as which User, Candidate or Customer it was.
This data helps us to understand how people use our platform, which can guide us in improving the platform.
It also helps us to monitor the security of the Platform.

We may also store your IP address as part of these logs.
This is in case of the event of unauthorized access to your account.
We work hard on maintaining the security of your data (see our [Security Policy](/security/)), but in the highly unlikely event of unauthorized access to your account, having the IP address stored with these logs will allow us to see if or how that unauthorized access was used.

**Feedback you send us.**
When you send us feedback, either via the Platform or by email, we store this information.
It helps us to understand how people use Our Services and to make improvements to them.


### If you are a Candidate, we may also collect from you:

**The inputs of your assessment.**
These are used to generate your results for the Customer whose access code you used to take the assessment.
The Customer does not see the order of the statements that you submitted.
For more information about how they use the results please refer to the [Candidate FAQ page](https://www.axiogrow.com/candidate-faqs).
We also use an anonymised version of this data for research purposes (see **Assessment Data**).

**Approximate date of birth, gender and approximate location.**
When a Candidate completes their assessment, we store their approximate location based on their IP address, and we may also collect their approximate date of birth and their gender.
These are used as part of the anonymised data which we use to conduct research.
We use them for research, and we may occasionally use them to see how different demographics use the Platform.
(See **Assessment Data**.)


If you are a User, we may also collect from you:

**The inputs of your self-awareness assessment.**
These are used to display your results to you. We may in the future use them to tailor parts of the platform or correspondence to you to make it more relevant and useful.
We also use an anonymised version of this data for research purposes (see **Assessment Data**).

**Approximate date of birth, gender and approximate location.**
When a user completes their self-awareness assessment, we store their approximate location based on their IP address, and we may also collect their approximate date of birth and their gender.
These are used as part of the anonymised data which we use to conduct research.
We use them for research, and we may occasionally use them to see how different demographics use the Platform.
(See **Assessment Data**.)

**Callback request details.**
If you request a phone call from us in relation to your self-awareness assessment results, we will store details of that request.
Those details include the phone number that you provide.
We will store the phone number until we have phoned you, after which we will delete it from our system.
We also ask our staff member who called you to delete it from their phone call history as well, although that relies on a fallible human and also the capabilities of their phone.

**Any data that you’ve explicitly submitted through the Platform.**
Many of the features in the platform (for example: adding a focus area, creating a goal, or taking notes on a piece of content) involve you explicitly entering information.
In the cases where you are explicitly entering data, you should assume that we are storing this.
We store this data primarily to make those features work.
We may also use this data to customise things in the platform for you or correspondence to you.
For example, if you’ve got 5 goals set up, we might avoid sending you tips on how to create a goal.


Assessment Data
------------------------------

We use the anonymised results of our assessment for both Candidates and Users for research purposes (for example, looking at whether wellbeing is improving over time in 20-30 year olds across the UK).
This research is always done on anonymous data.
We might sell the findings from our data, or we might make the findings public for the benefit of others in society to learn from, but no personal details ever even make it as far as the input data for the research.

The data that we conduct research on includes (if the users provided them to us) the approximate date of birth, gender and approximate location of the users.
Again, this data is entirely anonymous.


Other data that we may collect across all sites
-----------------------------------------------

**Correspondence with you.**
If you’ve corresponded with us using means other than our web platforms, for example by phone, email or (strangely) by post, then we may keep correspondence, including any personal information within it, so that we’ve got a record of our contact with you for future reference.

**Any other information that you’ve explicitly provided to us.**
Any other information that you've explicitly entered, indicated or selected on either Our Website or Our Platform.

**Error logs.**
When an error occurs on our platform we try to collect a log of the error, which includes information such as what time it happened, what URL the error occurred on, which line of code it occurred on, who the current user was (if the user is logged in) and what browser and operating system they were using. We don’t store the user’s details within the log itself, we just store the ID (a number) which identifies the user in our database so that we can potentially look up other parts of their data if it assists with debugging.
We try not to store any personal information with these logs, but if data was submitted with the request then that may be logged.
These logs are stored in Google Cloud Platform, from which they are deleted after 30 days.
Currently, some logs may also be emailed to our software engineers.
We’re trying to stop doing this, but it allows us better logging than Google Cloud Platform.

If we see that an error has occurred when you were using the Platform, then we may contact you to ask you for more information about what happened, or to inform you that the problem has been fixed. If you do not wish us to contact you in this way, you can reply to the correspondence or you can email us at [support@axiogrow.com](mailto:support@axiogrow.com).

**Timestamps.**
Most items that we store in our database (be that a purchase, a goal, a callback request, etc) include a timestamp of when they were created and when they were last modified.
We store these because they’re incredibly useful for a variety of purposes, for example, if you're a wellbeing User, displaying your goals steps to you in the order that you created them, or finding the most recent customers in our database to count our weekly sales.
We’re storing timestamps all over the place!

**Google Analytics data.**
We use Google Analytics across our sites to give us information about how our sites are being used.
This data is anonymous and has no link to any personal information or user accounts which we store.

**Facebook data.**
Facebook uses cookies to track users across the web.
We use some of Facebook’s services (e.g. advertising) to advertise our products, and those services may give us information about visitors to our site based on data that Facebook collects.

To learn more about cookies, see [www.aboutcookies.org](https://www.aboutcookies.org).


Data that we don't collect
--------------------------

We do not collect any of the following information from our web visitors, customers or users:

* Religion
* Sexual orientation
* Card details when a purchase is made. These are transmitted directly to our payment provider (Stripe) and are stored on their servers only with your billing address. We cannot access your card details.

Currently, our servers do not respond to the Do Not Track HTTP header.


Who can access your information
-------------------------------

### You can

Your data is, after all, yours. See **Your rights under GDPR**.


### We can, but we try not to

We try our best to avoid looking at personal data in our systems. But there are some occasions when we do. See When we access your information.


### Users

#### Your boss can’t

If you're a User and your Wellbeing Subscription is through your company/employer, then we take care to protect your personal data from them.
We will never show them your individual self-awareness results.
However, if you initially took the assessment as a Candidate, and have then been given access to your personal wellbeing hub, your employer will have seen some of your results already, as the hiring report and the self-awareness report are both based on the same assessment but using a slightly different set of measurements from it.
We’ll never tell your company/employer if you’ve requested a call-back to discuss your wellbeing results, and we’ll never show them the notes or goals that you’ve written in the Platform.

Here are the few things which we **do** allow them to see:

* The names and company email addresses of who’s registered on the Platform. This is generally for managing subscription places.
* Anonymised and aggregated reports of the self-awareness results across groups of Users.
* Potentially other anonymised and aggregated information, for example 72% of their staff have set themselves a goal in the Platform.

We take care to ensure that the wellbeing reporting which we provide to your company/employer doesn't compromise your anonymity.
To do this we enforce a minimum number of Users whose wellbeing data can be aggregated into a single report.
This number depends on the type of the report (e.g. average scores, distribution graph, correlation chart, etc).


### Candidates

#### Your prospective employer can

If you take the assessment as a Candidate, we will provide your name, email address and results to your prospective employer (the Customer who's access code you used).
For more information about how they use the results please refer to the [Candidate FAQ page](https://www.axiogrow.com/candidate-faqs).


When we access your information
-------------------------------

The servers and systems that we use may process your data in order to provide the AxioGrow Platform and Our Services to you.
In addition to that, we may have our humans look at your data for the following reasons:

* For general administration of the Platform and Our Services.
* To monitor the smooth running and security of the Platform. We look at our logs regularly to ensure that the Platform is running smoothly and that there isn’t any untoward or suspicious activity happening. These logs may occasionally contain bits of user data, which might get seen by humans.
* To diagnose and fix bugs. We try not to look at user data when doing this if at all possible, but sometimes there’s something specific about a particular user’s data which triggers a problem or we just really struggle to figure out an issue, in which case we may look at your data for the purpose of fixing the problem.
* To respond to support requests from you.
* When conducting a callback request about your self-awareness results.
* To improve the Platform. For example analysing which features are most or least used, which content is most or least popular, or how often users are returning to the Platform. We try to do this analysis without viewing any individual user data if we can help it.
* To create research from the anonymised assessment results.
* When required to by law.
* To send you correspondence.


What we won’t do with your data
-------------------------------

* For Users of the wellbeing features, we won’t give details of an individual’s results from the self-awareness assessment to the employer (or whatever organisation the User’s account is linked to).
* We won’t sell any personal data to third parties.


Where the data is stored and processed
--------------------------------------

To provide Our Services to you we use several third party service providers to run the Platform and its associated systems. In GDPR terms, these are called “data subprocessors”. We currently use:

**Google Cloud Platform.**
This provides and runs the main infrastructure for the Platform, including our servers, database(s), log storage, error reporting and file hosting.
The servers for the Platform are in Belgium.
The database for the Platform is in London.
You can read about Google Cloud Platform data security and privacy [here](https://cloud.google.com/security/privacy/).

**Stripe.**
This is our payment provider for taking your money!
Only on your request, of course.
This applies only to Customers.
You can read about Stripe’s data store [here](https://stripe.com/gb/privacy).

**SendGrid.**
This is our email sending provider which is used for sending both our ‘transactional’ emails which are triggered from the platform (e.g. “Here’s your verification link”) and our ‘marketing’ emails (e.g. “10 reasons why AxioGrow is so great”).
We do not currently send Candidate data to SendGrid.
You can read about SendGrid’s data storage [here](https://sendgrid.com/policies/security/).

**Gmail/Google Workspace.** This is our email provider for receiving emails and for sending ad-hoc emails written by our humans (e.g. replying to an email you’ve sent us). You can read about Google Workspace data security [here](https://workspace.google.com/security/).

**Our website** ([www.axiogrow.com](https://www.axiogrow.com)) is hosted by [Webflow](https://webflow.com/). When you sign up to our mailing list on the website, your details will be transmitted directly to SendGrid. When you make a payment on the website, your payment details will be transmitted directly to Stripe.


Deleting your data
------------------

You can ask us to delete your data at any time.
This right is also protected under the GDPR (see Your rights under GDPR).

For Candidates, if you would like your data to be deleted, please contact the Customer whom you took the assessment for.
(Note that your data will be automatically deleted after 12 months anyway.)

For anyone else, if you would like your data to be deleted, please contact us at [support@axiogrow.com](mailto:support@axiogrow.com).


### Data which takes time to delete

When you ask us to delete your data, it may take some time for your data to be fully deleted from all of our systems. We may still retain your personal data in:

* Backups of our Platform database, usually for 7 days.
* Email sending records (in SendGrid), usually for 7 days.
* Log files for our Platform, usually for 30 days.
* Payment records in our payment provider (Stripe). These may be retained indefinitely, as they may be necessary for our financial records.


### Data which we retain indefinitely

We may indefinitely store other data which was once linked to your personal data but which is now anonymised.
These pieces of data may still be linked to each other, so we know that they originated from the same person, but we will no longer know who that was:

* Assessment results.
* Records of actions taken on the platform (e.g. logins, page views, content ratings).

Notes you’ve typed and goals that you’ve written will not be retained in this way, as these may contain things personal to you, so we delete them.


Your rights under GDPR
----------------------

Under the EU General Data Protection Regulation (GDPR), you have several rights.
These are listed below along with our own brief descriptions and/or comments.
If you’d like the official legal definitions, please visit [gdpr-info.eu](https://gdpr-info.eu).

**The Right to Information.**
Broadly speaking, this is your right to know what, when and why we collect data from you and what we do with it.
In other words, your right to know the information in this policy.

**The Right of Access.**
You have a right to have a copy of the data that we hold about you.
It’s your data. You want it, you got it.

**The Right to Rectification.**
You have a right to have any mistakes in the data that we hold about you corrected.
Some of your data can be amended directly in the Platform.
To have any other data that we hold about you amended, please contact us by email (see below).

**The Right to Erasure.**
You have the right to have your data deleted from our systems.
If you request deletion of your data, please note the details in the **Deleting your data** section above.

**The Right to Restriction of Processing.**
You have the right to ask us to temporarily stop processing your data in certain situations.

**The Right to Data Portability.**
In certain situations you have the right to receive your data in a structured and machine-readable format.

**The Right to Object.**
You have a right to object to your data being processed, subject to some caveats.
This right probably applies most relevantly to asking us to stop sending you emails.
Most emails that we send you will have an ‘unsubscribe’ link to allow you to unsubscribe or set preferences for what types of emails you are happy to receive from us.
Some emails from us (for example, a confirmation email when you make a purchase) we consider to be essential, and so we consider to fall under the aforementioned caveats.

**The Right to Avoid Automated Decision-Making.**
We do not do any activities which we consider to “use systematic and extensive profiling with significant effects”, so this right is unlikely to be applicable to any of Our Services.

To exercise any of these rights, please email us at [support@axiogrow.com](mailto:support@axiogrow.com).


You got to the end! 🙌 Well done.
If you liked our privacy policy (don’t be afraid to admit it) please tell us, or shout about it on social media
[Instagram](https://www.instagram.com/axiogrow),
[Twitter](https://twitter.com/axiogrow),
[Facebook](https://www.facebook.com/axiogrow).


---------------------------------------

You can view the history of this document [here](https://gitlab.com/fidlleaf/terms-and-policies/-/commits/master/privacy-policy.md).
