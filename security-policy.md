
At AxioGrow we take data security seriously.
You could say we’re a little bit nerdy about it.
Here we outline how we work to keep your data secure, and what you can do to help protect it.


The truth
---------

If you see a website claiming to be “100% secure”, they’re being misleading.
Website security is not as simple as something which you have or don’t have, nor is it something which is ever finished.
It’s a continuous process.
It involves learning about hackers’ latest cunning tricks, adopting the latest protection strategies, installing updates, monitoring activity and always thinking about security in our work as we add features and improvements to the platform.

Below you’ll find some more info on how we do that. But first…


Your responsibilities
---------------------

### Password

Your access to the AxioGrow platform is protected by your password.
It is your responsibility to make sure that this password is **strong**, **unique** and **secret**.
If you’ve used that password for **any** other app or service, **ever**, or shared it with anyone else, then we cannot be held responsible for keeping your account secure.
No excuses, keep that password safe.

Can’t remember all those passwords? No one can.
Use a password manager.
We like [BitWarden](https://bitwarden.com/).

### Email

As is common with online accounts, we use your email address to allow you to unlock access to your AxioGrow account.
Therefore, if your email account isn’t secure, neither is your AxioGrow account, or half of the rest of your online life.

We recommend setting up 2 factor authentication on your email account.
See more info for
[Gmail](https://www.google.com/landing/2step/),
[Hotmail/Outlook](https://www.eff.org/deeplinks/2016/12/how-enable-two-factor-authentication-outlookcom-and-microsoft),
[Apple/iCloud](https://support.apple.com/en-us/HT204915).

We also recommend that you enter your email address into [haveibeenpwned.com](https://haveibeenpwned.com) to see if your email address has appeared in any known data breaches.


How we do things
----------------

This is a brief overview of how we protect your data


### We hold as little data as possible

The more data we store, the worse the fallout from a security breach is going to be.
See our [Privacy Policy](/privacy/) for how we handle your data like nuclear waste.


### We only do the bit we’re good at

Here at AxioGrow we know a thing or two about wellbeing, personal development and building great web applications.
But we’re not experts in configuring the complicated giggery-wotsit-thingies on packet-inspection network firewalls, or that sort of stuff.
So when it comes to the infrastructure for running our platform, we use the people who are experts at it.

Our platform is all run on Google Cloud Platform.
Some of the world’s leading web security experts work for Google, which means that sometimes they’ve discovered and patched a security flaw before it’s even become public.

We stick to the stuff that we’re good at.


### HTTPS

Both our website ([www.axiogrow.com](https://www.axiogrow.com)) and our platform ([app.axiogrow.com](https://app.axiogrow.com)) are served over HTTPS, which means that everything which is transferred between you and us and vice versa is encrypted while it’s in transit.
Even if you’re riding on the free (and insecure) Starbucks wifi, your connection to us remains secure.
Check your browser’s address bar to make sure.


### Other tools

We use a variety of latest best practices, tools and techniques to protect the security of your data, as well as keeping our software tools and libraries up to date.


Data Breaches
-------------

We’ve never had a data breach yet.
But if we do, here’s how we’ll handle it:

* We promise to take reports of data breaches seriously.
* As soon as we know a data breach to have occurred, we promise to take swift action to protect users’ accounts.
* We promise to be honest and to disclose data breaches to our users and the public as swiftly as possible.

Under the GDPR we are obliged to report certain data breaches to both users and to the Information Commissioner’s Office.


### How to disclose a breach or security issue to us

Email us at [security@axiogrow.com](mailto:security@axiogrow.com).
In the very unlikely event that we don’t respond within 24 hours then please badger us on [Twitter](https://twitter.com/axiogrow).
Please don’t disclose any details publicly though, just use Twitter to nudge us about the email.


### Conducting Security Research

If you are trying to find or verify a potential security vulnerability in our systems, please do so responsibly. This means:

* Do not try to access or modify another user’s data. Test things against your own account.
* Do not try to destroy any data.
* Do not do anything which may make our systems unusable or unreachable by other users (for example flooding our systems with web traffic).

If you want to test something but cannot do so without breaching these guidelines then please contact us at [security@axiogrow.com](mailto:security@axiogrow.com) and if appropriate we will provide you with test accounts or systems to test against.


### Bug Bounty Program

Some companies have “bug bounty” programs, which offer cash rewards to people who find and report security vulnerabilities in their systems.
We don’t have large quantities of cash to offer, but if you responsibly report a “good” vulnerability to us then we might send you a
[Bounty bar](https://duckduckgo.com/?q=bounty+chocolate+bar&t=h_&iax=images&ia=images)
as a gesture of our appreciation.


You can view the history of this document [here](https://gitlab.com/fidlleaf/terms-and-policies/-/commits/master/security-policy.md).
