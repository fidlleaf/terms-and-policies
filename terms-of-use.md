Thank you for using AxioGrow! We hope that you enjoy using our Platform.
We believe that if you use it in a normal and reasonable way that the clauses in this document won’t ever need to be enforced.
But to make things clear and to ensure that we can run our services effectively, we set out these Terms of Use.


The Short But Not Legally Binding Version
-----------------------------------------

If you just want a quick summary without reading the whole thing, here’s a version which has no legal meaning but which we think is a reasonable abbreviation:

* We’re making changes to the Platform all the time. Things will change, there might be bugs.
* Occasionally the Platform might go down. But we try hard to keep it up.
* We collect and store data from/about you. See our [Privacy Policy](/privacy/) for info, including who can see the data.
* Use a strong, unique password for your account, if you have one. Keep your email account secure.
* Please don’t abuse the Platform or our staff. If you do we’ll disable your account.
* We don’t claim copyright over the things you write in the platform. But we do claim the right to an anonymised copy of your assessment results, and we store various stats and other data to run the Platform.
* You can’t claim copyright over any of our stuff (our wording, logos, etc).
* There is third party content in the wellbeing platform (videos from YouTube, etc). You don’t own it, we don’t own it, you might not like all of it.
* We’re not liable for any bad stuff that happens (directly or indirectly) as a result of our Platform or services.


The Full Version
----------------

This document sets out the terms of use for the Platform (see **Definitions**). By using the Platform you are agreeing to these Terms of Use.


Definitions
-----------

Here’s what mean when we use these words or terms in this document:

* **Us/we//our/AxioGrow**: AxioGrow Ltd, the UK company with registered company number 12364269 and registered address 227a West Street, Fareham, England, PO16 0HZ.
* **Customer**: someone who has made a purchase with us. This is the person who pays for one or more Subscriptions.
* **User/you**: someone who uses the AxioGrow Platform. This may or may not be the same person as the Customer.
* **Platform**: the web application which is available at <app.axiogrow.com>.
* **Subscription**: this is what a Customer purchases; it allows a User to access the Platform for a specific period of time.


Bugs, Features & Changes
------------------------

We want the AxioGrow platform to be as good as it can be, so that you’ll tell your friends, colleagues and social media followers about us.
But as with any software which has a lot of users, it’s impossible for it to perfectly please everyone.
So we make no guarantee that the Platform will meet your exact expectations.
Although we hope that it will exceed them.

Given the ever-evolving nature of the Platform, we reserve the right to change or discontinue, temporarily or permanently, any part of the Platform at any time, with or without notice.

As with all software, there are going to be bugs.
We take pride in the AxioGrow Platform; our reputation and the continuing existence of our business rests upon it.
We are continually working to improve it, balancing the priorities of adding new features, improving existing features, fixing bugs and maintaining its security.
But despite our rigorous automated testing and diligence, some bugs will inevitably occur.

We don’t provide refunds for the occurrence of bugs and we do not accept liability for any damages or loss of profit or income, whether directly or indirectly caused by our systems, except for where we have taken payment in error.

If you find a bug or something that you think could be improved, please tell us about it at [support@axiogrow.com](mailto:support@axiogrow.com).


Uptime
------

We aim to have the Platform available online all day every day.
However, it may occasionally be unavailable due to:

* Us performing planned updates to the platform (these are usually seamless, but they occasionally require a short amount of downtime).
* Us getting things wrong.
* Factors out of our control, such as downtime of one or more of the services that the Platform relies on, such as Google Cloud Platform.

We do not offer monetary compensation for downtime of the Platform.
But we may choose to do so at our discretion as a gesture of goodwill if there is downtime which we deem to have had an unreasonable impact on you.


How We Use Your Data
--------------------

Information about how we use your data is available in our [Privacy Policy](/privacy/).
Your use of the Platform is subject to - and constitutes - acceptance of this policy.


Account Security
----------------

We put a lot of effort into keeping your account secure, this includes a variety of measures including logging, monitoring, rate limiting, encryption, restrictions to prevent overly short or common passwords, making use of modern web security techniques, automated testing and diligence in our coding practices.

You are responsible for the security of your login details and your email account, as detailed in our [Security Policy](/security/).


Reasonable Use
--------------

Please don’t bombard our site with lots of HTTP requests or attempt to gain unauthorised access to data in our Platform.


Right to Disable Accounts Due to Misuse
---------------------------------------

We reserve the right to disable any account and/or suspend any Subscription on our Platform due to misuse or abuse.
We deem misuse or abuse to include (by not be limited to) any of the following:

* Making insulting, obscene, derogatory, threatening or abusive remarks towards or about us or our staff, via any communication channel, not just the Platform.
* Using the platform to facilitate, aid or attempt any kind of crime.
* Attempting to access another User’s data or any of Our data which we do not explicitly make available to you.
* Attempting to sign up using another user’s access code or email address.

In the case of abuse we may disable that User or Customer’s account indefinitely and without notice.
In such cases, no refund will be given.


Copyright
---------

We do not claim ownership over the written material that you create in the platform.
But we do store it.

You must not submit content to the Platform which you are not the creator of.

We maintain copyright of the data which we generate as a result of actions taken on the Platform by Users.

The AxioGrow name, logo, look and feel of the Platform are all copyright or design right to us and/or are protected by trade mark and/or are registered on Companies House.
All rights reserved.

You agree not to reproduce, duplicate, copy, sell, resell, distribute or embed any part of the Platform or the AxioGrow website (at [www.axiogrow.com](https://www.axiogrow.com)).

You must not use the AxioGrow name, logo, look & feel, brand or platform for promotional purposes or associate them with other products, brands or people without first receiving explicit permission from us.
You can request permission by emailing us at [hello@axiogrow.com](mailto:hello@axiogrow.com).


Third Party Content
-------------------

The Platform contains content from a variety of third party creators, embedded via a variety of third party platforms.
In respect to this content:

* We do not claim any intellectual property rights over it.
* Its inclusion in the Platform does not give you any intellectual property rights over it.
* Not all of the content will be to the liking of every User. Despite our process for vetting content, we do not take any responsibility for any offence, grievances or other consequences which may arise as a result of this content.


Liability
---------

We will not be liable for any loss or damages whatsoever to any User, Customer or third party resulting from:

* Use or non-use of the Platform.
* Malfunction or unavailability of the Platform.
* Alteration, corruption, interception or unauthorised access to data.
* A breach of contract (either these terms or other), tort, or any other matter relating to the services of AxioGrow Ltd.

In other words, when you use the AxioGrow Platform you are putting your trust in us.
Read our [Privacy Policy](/privacy/) and [Security Policy](/security/) to find out why you should.


-------------------------------------

You can view the history of this document [here](https://gitlab.com/fidlleaf/terms-and-policies/-/commits/master/terms-of-use.md).
