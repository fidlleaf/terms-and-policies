Definitions
-----------

Here’s what mean when We use these words or terms in this document:

* **Us/We/AxioGrow**: AxioGrow Ltd, the UK company with registered company number 12364269 and registered address 227a West Street, Fareham, England, PO16 0HZ.
* **Customer/you**: a person or organisation who makes a purchase with us. This is the entity which pays for a Subscription, possibly alongside other services from us.
* **User**: someone who uses the AxioGrow Platform. This may or may not be the same person as the Customer.
* **Candidate** a type of User whose assessment results may be shown to a Customer in the Platform's hiring features.
* **Platform**: the web application which is available at <app.axiogrow.com>.
* **Our Website**: our website which is available at <www.axiogrow.com>.
* **Subscription**: this is what a Customer purchases; it may be a:
    - **Wellbeing Subscription**, which  allows a numberset of Users to access the Platform’s wellbeing features for a specific period of time;.
    - **Hiring Subscription**, which allows a number of Users to take the assessment and have their results displayed to the Customer via the platform’s hiring features.
* **Credit**: a token or counter which can be purchased to allow a Customer to view the results of a single assessment by a Candidate.

Subscription Agreement
----------------------

When you purchase an AxioGrow Subscription or Credit(s), the use of our Platform and our Website by both you and the Users of your Subscription is subject to our
[Privacy Policy](/privacy/),
[Cookies Policy](/cookies/) and
[Terms of Use](/terms-of-use/).


Workshop Purchases
------------------

If you are buying one or more workshops or consultancy from us, then We will send a separate agreement to cover the terms of the work.


Company Wellbeing Reports
-------------------------

Each Wellbeing Subscription includes the option for one or more of your Users to have access to your 'company hub'.
This is an area of the Platform which provides anonymised reports on the wellbeing results of the Users in your Subscription, plus other wellbeing resources and information about your Subscription.
We expect the User(s) whom are granted access to the company hub to be your HR staff or senior managers.
We take the anonymity of Users' results seriously, and to protect this anonymity We require that a minimum number of Users in your Wellbeing Subscription have taken the assessment before each report is shown.
The minimum number varies per report, depending on the level of detail that it reveals.


Subscription Period, Pricing and Billing
------------------------------------

For Hiring Subscriptions you are billed on a monthly basis and can cancel at any time.
Credits in your monthly allocation do not carry over to the next month.
For ad hoc hiring reports you can buy Credits as and when you need them, with no expiry.

Wellbeing Subscriptions are for a minimum period of 12 months.
The monthly cost of a Wellbeing Subscription is based on the number of Users who have accessed the platform under that Subscription.
Unless you have agreed with Us otherwise, the pricing is as follows:

* 0-50 Users: £89/month
* 51-100 Users: £169/month
* 101-150 Users: £249/month
* 151-200 Users: £329/month
* 201-300 Users: £479/month
* 301-400 Users: £629/month
* 401-500 Users: £769/month
* More than 500 Users: price to be agreed directly.

All Subscriptions will be invoiced on a monthly basis upfront.
Invoices must be paid within 28 days of receipt.
We reserve the right to withhold report allocation if invoices are not paid.

If you do not receive an invoice from Us this does not exempt you from the charges.
If We fail to issue an invoice (which We will try very hard not to!) We may still invoice for that month's charges for up to 6 months after the end of that month's billing period.
If you do not receive an invoice due to it being filtered by an email filter or You having not notified Us in writing of a change of email address, you are still liable for the charges.
If you don't receive an invoice from Us in any month, please notify Us as soon as possible and together we'll work out what's gone wrong.


Access Codes
------------

When you start any Subscription with us, We will provide you with an access code to allow your Users or Candidates to access the Platform.
This access code is unique to your Subscription, and it is your responsibility to ensure that it is not shared with anyone whom you do not wish to access the Platform or take the assessment under your Subscription.
For a Wellbeing Subscription, You can choose to ask Us to restrict your access code so that only Users whose email addresses have a particular ending after the @ symbol can register to your Wellbeing Subscription.

If your company does not have its own email domain and you believe that a User who is not a member of your staff has gained access to your Subscription through the leaking of the access code, then provided We believe this to be a genuinely unauthorised User, We will remove them from your Subscription and your future billing, and will issue you a new access code.


Cancellation
------------

Hiring subscriptions can be cancelled at any time or upgraded/downgraded.
Changes will be reflected in the next month’s billing and report allocation.
If you cancel you will still receive the reports allocated in your subscription for that month.
The platform will remain ‘live’ and you can revert to pay-as-you-go or upgrade/downgrade your Subscription at any time.

All AxioGrow Wellbeing Subscriptions are subject to a minimum 12 month contract.
After this initial 12 month period, the Subscription is on a rolling monthly basis.
Your Subscription will remain active and We will continue to bill you for it until you ask Us to cancel it.

Cancellation must be requested in writing (by email to support@axiogrow.com if digitally) at least 14 days before the start of the next monthly billing period.
Note that your monthly billing period may run from any day of the month (based on the day of the month on which your Subscription started), and does not necessarily align with when you receive your invoice.

For example, if your monthly billing period starts on the 17th of the month, you must give notice of cancellation by the 3rd of the month in order to avoid activating the next billing period.


Refunds
-------

Once you've entered into a Wellbeing Subscription Agreement, you may change your mind up to 14 days after the agreed Subscription start date.
In this case, so long as no Users have registered on the Platform under the Subscription, no billing will occur.
If any Users have registered on the Platform under the Subscription, you will incur the first month's billing at the monthly rate according to the prices listed above.

For Hiring Subscriptions and Credits purchased for hiring reports, we do not offer refunds, but you can cancel your Hiring Subscription at any time.


Re-Taking the Self Awareness Assessment
---------------------------------------

One of the key features of AxioGrow is our self-awareness assessment.
A Subscription allows each User to take this assessment once every 6 months.
(The behaviours and traits which the assessment measures are not likely to change over a very short period of time, so allowing Users to take the assessment once a month would probably not be useful.)


Removing Users from Wellbeing Subscriptions
-------------------------------------------

After the minimum 12 month Wellbeing Subscription period is up, you may notify us that some of your Users have left the organisation and are no longer part of your staff.
We will then mark these Users as 'leavers' and remove them from the pricing calculation in the subsequent billing periods.


Right to Disable Accounts Due to Misuse
---------------------------------------

We reserve the right to disable any account and/or suspend any Subscription on our Platform due to misuse or abuse.
We deem misuse or abuse to include (by not be limited to) any of the following:

* Making insulting, obscene, derogatory, threatening or abusive remarks towards or about Us or our staff, via any communication channel, not just the Platform.
* Using the Platform to facilitate, aid or attempt any kind of crime.
* Attempting to access another User’s data or any of our data which We do not explicitly make available to you. (See our [Security Policy](/security/).)
* Attempting to sign up using another User’s access code or email address.

In the case of abuse We may disable that User or Customer’s account indefinitely and without notice.
In such cases, no refund will be given.


Right to Refuse Sale
--------------------

We reserve the right to refuse the sale of any of our products (Subscriptions or other services) to anyone at any time for any reason.
We don’t intend to exercise this right, but it’s here just in case.


Got Questions?
--------------

If anything in these Terms of Purchase aren’t clear, you’ve got a suggestion for improvement, or you’d just like to talk to us, you can reach Us on <hello@axiogrow.com>.


You can view the history of this document [here](https://gitlab.com/fidlleaf/terms-and-policies/-/commits/master/terms-of-purchase-organisations.md).
