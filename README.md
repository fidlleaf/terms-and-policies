FidlLeaf Terms and Policies
===========================

Here are the terms & conditions and other policies for axiogrow.com, showing the history of how they have changed over time.

The policies are in Markdown format, which means that the formatting looks slightly different to how it is on our website, but the content is the same.


Modification Dates
------------------

When viewing the history of files in this repository, note that the modification dates/times are when the changes were made by our developers, and don't necessary correspond _exactly_ with when the changes were deployed to our site.
