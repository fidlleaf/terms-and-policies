This Privacy Policy applies to all websites under the [axiogrow.com](https://axiogrow.com) domain name, including [www.axiogrow.com](https://www.axiogrow.com) and [app.axiogrow.com](https://app.axiogrow.com).

We use cookies on our websites. You’ll find a notice about this on our homepage as well, which also links back to this policy.
If you use this website after we’ve displayed the notice to you, we will treat this as your consent to our use of cookies for the purposes that we’ve explained below in our policy.


1. Information about cookies
----------------------------

A cookie is a small text file that is placed onto your computer (or other electronic device) when you access our website.

There are 4 different types of cookies:

a. Strictly-necessary cookies (required for the website to run). These allow you to do things like log in securely.
b. Performance cookies (which measure how many visitors use our site, which pages they visit and how they interact with the site). These help us to improve our website.
c. Functionality cookies (which remember specific visitor’s preferences for the way they use our site, e.g. language settings or font size).
d. Targeting/advertising cookies (these collect information about individual visitors that enable us, or third parties, to provide relevant advertisements to those who have visited our site).

Our website may use each type of cookie in order to:

a. Make your online experience more efficient and enjoyable
b. Improve our services
c. Recognise you whenever you visit this website and distinguish you from other users of our site
d. Obtain information about your preferences and use of our site
e. Provide you with advertising that is tailored to your interests
f. Carry out research and data analysis to help us to improve our content and services and to better understand our customer preferences and interests

Your web browser places cookies on your hard drive for record-keeping purposes and sometimes to track information about you.

We have set out specific details of the cookies we use on our site in section 4 below.


2. Third-party cookies
----------------------

Like many other websites, third parties may also set cookies on our website. These third parties are responsible for the cookies they set on our site and we have no control over them. For further information on how their cookies policies work, please visit their websites.


3. Managing cookies
-------------------

You can manage cookies yourself.

You can set your web browser to refuse cookies, delete cookies, or to alert you when cookies are being sent.

You will need to visit the web browser’s site to manage, but for ease here are links for some popular browsers:
[Google Chrome](https://support.google.com/accounts/answer/61416?co=GENIE.Platform%3DDesktop&hl=en),
[Firefox](https://support.mozilla.org/en-US/kb/enable-and-disable-cookies-website-preferences),
[Microsoft Edge](https://docs.microsoft.com/en-us/microsoft-edge/devtools-guide-chromium/storage/cookies),
[Apple Safari](https://support.apple.com/kb/ph21411?locale=en_US).

For further information about cookies and how to manage or disable them please go to: [www.aboutcookies.org](https://www.aboutcookies.org).

Please also note that if you change/block cookies then:

* some parts of our website may not function properly or as well as they otherwise might;
* changes will apply across all websites that you visit (unless you choose to block cookies only from particular websites); and
* the settlings that you change will only apply on the device on which you change the settings and will not apply across all other devices that you use.


4. Specific details
-------------------

We have set out in the table below details of each of the cookies on our site, a description of what they do and, where relevant, external links that provide more information about them:

| Cookie Name      | Description |
| :--------------- | :---------- |
| Session cookie     | We use a cookie to persist your identity/login from one page to the next. Without this, you would need to log in again on every page. This cookie is linked to your individual user account on our site, and that user account may then be linked to any other data that we store about you. |


--------------------

You can view the history of this document [here](https://gitlab.com/fidlleaf/terms-and-policies/-/commits/master/cookies-policy.md).
