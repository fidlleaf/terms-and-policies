Definitions
-----------

Here’s what mean when we use these words or terms in this document:

* **Us/we/AxioGrow**: AxioGrow Ltd, the UK company with registered company number 12364269 and registered address 227a West Street, Fareham, England, PO16 0HZ.
* **Customer/you**: someone who has made a purchase with us. This is the person who pays for one or more Subscriptions.
* **User**: someone who uses the AxioGrow Platform. This may or may not be the same person as the Customer.
* **Platform**: the web application which is available at <app.axiogrow.com>.
* **Subscription**: this is what a Customer purchases; it allows a User to access the Platform for a specific period of time.


The terms set out in this document apply to purchases of products referred to on our website as being for individuals.
For purchases of products referred to as a ‘company subscription’ or ‘company package please see our
[Terms of Purchase for Companies](/terms-of-purchase-organisations/).
If you’re not sure which set of terms applies to your purchase, please ask us at <hello@axiogrow.com>.


Purchasing
----------

When you make a purchase, you are buying one or more Subscriptions, each of which is for a user for a set period of time.
Your purchase and use of our Website is subject to our
[Privacy Policy](/privacy/) and
[Cookies Policy](/cookies/), and the Users’ access to the Platform is subject to our
[Terms of Use](/terms-of-use/),
[Privacy Policy](/privacy/) and
[Cookies Policy](/cookies/).


Subscription Activation & Start Date
------------------------------------

A Subscription is activated either when the user sets up their account by completing the account setup process in the Platform, or 30 days after the date when the Subscription was purchased, whichever is sooner.

The duration of a Subscription runs from the moment it is activated (the usual duration is 12 months).

If you have purchased multiple Subscriptions, the activation of one does not affect the other.

If you have purchased a free subscription, that subscription has been activated (as above), and you have then then upgraded it to a paid subscription, its activation date will be set to the time of the upgrade.


Refunds
-------

If you have purchased a Subscription in error, or you’ve just changed your mind and would like a refund, we will happily refund your purchase up to 14 days after the purchase date, so long as none of the Subscriptions in the purchase have been activated.


Renewals and Cancellation
-------------------------

When you purchase a subscription from us, that subscription renews automatically at the end of its duration for an additional period equal in length to the expiring subscription duration. You can cancel the subscription renewal at any time, either via the Platform or by contacting us on <support@axiogrow.com>.
When you cancel your subscription, access to the Platform for the User on that subscription will continue until the end of the current subscription period.

We will aim to send at least one reminder to the email address which we have stored for the Customer at least a week before the Subscription renews.
However, partly due to the varying deliverability of emails, we don’t guarantee that that email will reach your inbox.

After the Subscription payment is taken you can request a refund for up to 14 days after the renewal date, so long as the User has not retaken the self-awareness assessment since the renewal.


Transferring Accounts or Subscriptions
--------------------------------------
The User of a subscription can transfer their Platform account to a different email address at any time.
This does not change or erase the history of the account - any previous self-awareness results, goals and notes will remain, so changing the email address should not be used to transfer the account to a different person.
To get an account for a different User, you need to make a separate purchase.


Re-Taking the Self Awareness Assessment
---------------------------------------

One of the key features of AxioGrow is our self-awareness assessment.
A Subscription allows each User to take this assessment once every 6 months.
(The behaviours and traits which the assessment measures are not likely to change over a very short period of time, so allowing Users to take the assessment once a month would not be useful.)


Right to Disable Accounts Due to Misuse
---------------------------------------

We reserve the right to disable any account and/or suspend any subscription on our Platform due to misuse or abuse.
We deem misuse or abuse to include (by not be limited to) any of the following:

* Making insulting, obscene, derogatory, threatening or abusive remarks towards or about us or our staff, via any communication channel, not just the Platform.
* Using the platform to facilitate, aid or attempt any kind of crime.
* Attempting to access another User’s data or any of Our data which we do not explicitly make available to you. (See our [Security Policy](/security/).)
* Attempting to sign up using another user’s access code or email address.

In the case of abuse we may disable that User or Customer’s account indefinitely and without notice. In such cases, no refund will be given.


Right to Refuse Sale
--------------------

We reserve the right to refuse the sale of any of our products (Subscriptions, Company Packages or other services) to anyone at any time for any reason.
We don’t intend to exercise this right, but it’s here just in case.


Got Questions?
--------------

If anything in these Terms of Purchase aren’t clear, you’ve got a suggestion for improvement, or you’d just like to talk to us, you can reach us on <hello@axiogrow.com>.


You can view the history of this document [here](https://gitlab.com/fidlleaf/terms-and-policies/-/commits/master/terms-of-purchase-individuals.md).
